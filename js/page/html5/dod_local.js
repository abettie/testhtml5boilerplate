(function () {
    var dp = null;
    var prog = null;
    var info = null;
    var cont = null;
    var abrt = null;
    var reader = null;
    window.addEventListener("load", function () {
        // ドロップ領域
        dp = document.getElementById('dod-local-drop-area');
        // 進捗表示
        prog = document.getElementById('dod-local-prog');
        // ファイル情報
        info = document.getElementById('dod-local-info')
        // ファイル内容表示
        cont = document.getElementById('dod-local-cont')
        // 読み取り中止ボタン
        abrt = document.getElementById('dod-local-abrt');

        dp.addEventListener('dragover', function (evt) {
            evt.preventDefault();
        }, false);
        dp.addEventListener('drop', function (evt) {
            evt.preventDefault();
            file_droped(evt);
        }, false);
        abrt.addEventListener('click', abort, false);
    }, false);

    /*
     * ファイルがドロップされた時
     */
    function file_droped(evt) {
        // 表示領域をクリア
        info.innerHTML = '';
        prog.innerHTML = '';
        cont.innerHTML = '';

        // ドロップされたファイルのFileインタフェースオブジェクト
        var file = evt.dataTransfer.files[0];
        if (!file) {
            info.innerHTML = '<p>ファイルをドロップして下さい。</p>';
            return;
        }
        // FileReaderインタフェースオブジェクト
        reader = new FileReader();
        // ファイルロードの進捗表示
        reader.onprogress = function(evt) {
            if(evt.lengthComputable == true && evt.total > 0) {
                var rate = (evt.loaded * 100 / evt.total).toFixed(1);
                var msg = '';
                msg += evt.total + 'バイト中、' +
                    evt.loaded + 'バイトをロードしました。' +
                    '(' + rate + '%)';
                prog.innerHTML = msg;
            }
        }

        // ファイル情報を表示
        var msg = '';
        msg += 'size: ' + file.size + '<br>';
        msg += 'name: ' + file.name + '<br>';
        msg += 'type: ' + file.type + '<br>';
        info.innerHTML = msg;

        if (/^image/.test(file.type)) {
            // 画像ファイルの場合、表示する。
            reader.readAsDataURL(file);
            reader.onload = function (evt) {
                var el = document.createElement("object");
                el.setAttribute("type", file.type);
                el.setAttribute('data', reader.result);
                cont.appendChild(el);
            };
        } else if (/^video/.test(file.type)) {
            // ビデオファイルの場合
            var el = document.createElement('video');
            if (!/^(maybe|probably)/.test( el.canPlayType(file.type) )){
                return;
            }
            reader.readAsDataURL(file);
            reader.onload = function (evt) {
                el.setAttribute('type', file.type);
                el.setAttribute('src', reader.result);
                el.setAttribute('controls', true);
                cont.appendChild(el);
            };
        } else if (/^text/.test(file.type)) {
            // テキストファイルの場合
            reader.readAsText(file);
            reader.onload = function (evt) {
                cont.appendChild(document.createTextNode(reader.result));
            }
        } else {
            // 上記以外なら先頭の40バイト分を16進数表示
            reader.readAsBinaryString(file);
            reader.onload = function (evt) {
                var str = '';
                for ( var i=0; i<80; i++) {
                    var hex = reader.result.charCodeAt(i).toString(16);
                    if (hex.length < 2) {
                        hex = '0' + hex;
                    }
                    str += hex;
                    if ( (i+1) % 16 == 0) {
                        str += '<br>';
                    } else {
                        str += '&nbsp;';
                    }
                }
                cont.innerHTML = str;
            };
        }
    }

    function abort() {
        if (reader == null) { returnl; }
        reader.abort();
    }
})();