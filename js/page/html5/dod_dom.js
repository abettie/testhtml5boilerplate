document.addEventListener('DOMContentLoaded', function(){
    // イメージ領域
    var els = document.querySelectorAll('#dod-dom-src-area .item');
    for (var i=0; i<els.length; i++) {
        els[i].addEventListener('dragstart', function (evt) {
            console.log(evt);
            var elm = evt.target;
            evt.dataTransfer.setData('Text', elm.id);
            evt.stopPropagation();
        }, false);
    }

    // ドロップ領域
    var droparea = document.getElementById("dod-dom-drop-area");
    droparea.addEventListener('dragenter', function (evt) {
        evt.preventDefault();
    }, false);
    droparea.addEventListener('dragover', function (evt) {
        evt.preventDefault();
    }, false);
    droparea.addEventListener('drop', function (evt) {
        var elm = evt.target;
        var id = evt.dataTransfer.getData('Text');
        var target = document.getElementById(id);
        if (target) {
            droparea.appendChild(target);
        }
        evt.preventDefault();
    }, false);
}, false);
