<?php
/**
 * Created by PhpStorm.
 * User: toshi
 * Date: 2018/01/15
 * Time: 10:38
 */

define('TPL_PATH', INC_PATH.'/template');

define('ROOT_URL', '/TestHTML5Boilerplate');
define('CSS_URL', ROOT_URL.'/css');
define('JS_URL', ROOT_URL.'/js');
define('IMG_URL', ROOT_URL.'/img');
