<?php
/**
 * Created by PhpStorm.
 * User: toshi
 * Date: 2018/01/15
 * Time: 10:45
 */
?>
<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="<?= CSS_URL ?>/normalize.css">
    <link rel="stylesheet" href="<?= CSS_URL ?>/main.css">
    <script src="<?= JS_URL ?>/vendor/modernizr-3.5.0.min.js"></script>
    <script src="<?= JS_URL ?>/plugins.js"></script>
    <script src="<?= JS_URL ?>/main.js"></script>
</head>
<body>
<header>
    <ul>
        <li><a href="<?= ROOT_URL ?>">TOP</a></li>
        <li><a href="<?= ROOT_URL ?>/page/html5/dod_dom.php">DodDOM</a></li>
        <li><a href="<?= ROOT_URL ?>/page/html5/dod_local.php">DodLocal</a></li>
    </ul>
</header>
