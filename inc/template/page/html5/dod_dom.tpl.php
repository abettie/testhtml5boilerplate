<?php
/**
 * Created by PhpStorm.
 * User: toshi
 * Date: 2018/01/15
 * Time: 19:06
 */
?>
<link rel="stylesheet" href="<?= CSS_URL ?>/page/html5/dod_dom.css">
<script src="<?= JS_URL ?>/page/html5/dod_dom.js"></script>
<div class="container">
    <div class="dod-dom">
        <h1>Drag and drop a DOM.</h1>
        <div id="dod-dom-src-area" class="image-box">
            <img src="https://placehold.jp/100x100.png?text=A" id="dod-dom-item1" class="item">
            <img src="https://placehold.jp/100x100.png?text=B" id="dod-dom-item2" class="item">
            <img src="https://placehold.jp/100x100.png?text=C" id="dod-dom-item3" class="item">
            <img src="https://placehold.jp/100x100.png?text=D" id="dod-dom-item4" class="item">
            <img src="https://placehold.jp/100x100.png?text=E" id="dod-dom-item5" class="item">
        </div>
        <div id="dod-dom-drop-area" class="image-box"></div>
    </div>
</div>
