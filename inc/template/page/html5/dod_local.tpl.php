<?php
/**
 * Created by PhpStorm.
 * User: toshi
 * Date: 2018/01/15
 * Time: 19:17
 */
?>
<link rel="stylesheet" href="<?= CSS_URL ?>/page/html5/dod_local.css">
<script src="<?= JS_URL ?>/page/html5/dod_local.js"></script>
<div class="container">
    <div class="dod-local">
        <h1>Drag and drop a local file.</h1>
        <div id="dod-local-drop-area" class="drop-area">
            <div id="dod-local-prog">ここにデスクトップ上のファイルをドロップして下さい。</div>
            <div>
                <button id="dod-local-abrt">読み取り中止</button>
            </div>
            <div id="dod-local-info"></div>
            <div id="dod-local-cont" class="cont"></div>
        </div>
    </div>
</div>
