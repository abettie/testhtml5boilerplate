<?php
/**
 * Created by PhpStorm.
 * User: toshi
 * Date: 2018/01/15
 * Time: 10:37
 */
define('ROOT_PATH', dirname(dirname(__FILE__)));
define('INC_PATH', ROOT_PATH.'/inc');

require_once INC_PATH.'/const.inc.php';
require_once INC_PATH.'/func.inc.php';